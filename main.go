package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"github.com/gookit/color"
	"gitlab.com/codingpaws/dockerfile-yml/generator"
	"gitlab.com/codingpaws/dockerfile-yml/parser"
	"gitlab.com/codingpaws/dockerfile-yml/util"
)

const VERSION = "0.1"

var yellow = color.FgYellow.Darken().Render
var blue = color.FgBlue.Render

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("%s %s\n", blue("dockerfile-yml"), yellow(VERSION))
		fmt.Println()
		fmt.Printf("%s\n", yellow("Description:"))
		fmt.Println("  Converts your dockerfile.yml to a valid Dockerfile")
		fmt.Println()
		fmt.Printf("%s\n", yellow("Usage:"))
		fmt.Println("  dockerfile-yml <directory>")
		os.Exit(0)
	}

	pathDockerfileYaml := path.Join(os.Args[1], "dockerfile.yml")
	pathDockerfileTarget := path.Join(os.Args[1], "Dockerfile")

	yamlDockerfile, err := parser.Parse(pathDockerfileYaml)

	if err != nil {
		util.LogFailuref("could not parse YAML in %s: %s", yellow(pathDockerfileYaml), err)
		os.Exit(1)
	}

	dockerfile, err := generator.Generate(yamlDockerfile)

	if err != nil {
		util.LogFailuref("could not generate Dockerfile: %s", err)
		os.Exit(1)
	}

	ioutil.WriteFile(pathDockerfileTarget, []byte(dockerfile), 0664)

	util.LogSuccessf("generated %s (from %s)", yellow(pathDockerfileTarget), yellow(pathDockerfileYaml))
}
