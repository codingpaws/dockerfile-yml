package generator

import (
	"fmt"
	"strings"

	"gitlab.com/codingpaws/dockerfile-yml/commands"
	"gitlab.com/codingpaws/dockerfile-yml/parser"
	"gitlab.com/codingpaws/dockerfile-yml/util"
)

func Generate(yamlDockerfile parser.YamlDockerfile) (string, error) {
	generator := dockerfileGenerator{yaml: yamlDockerfile}

	return generator.ToString()
}

type dockerfileGenerator struct {
	yaml  parser.YamlDockerfile
	lines []string
}

func (generator *dockerfileGenerator) ToString() (string, error) {

	generator.write("FROM", generator.yaml.From)
	generator.writeEmptyLine()

	generator.writeVariables()
	generator.writeLabels()
	generator.writeWorkdir()
	err := generator.writeCommands()
	if err != nil {
		return "", err
	}
	err = generator.writeEntrypoint()
	if err != nil {
		return "", err
	}

	return strings.Join(generator.lines, "\n"), err
}

func (generator *dockerfileGenerator) writeWorkdir() {
	if generator.yaml.Workdir != "" {
		generator.write("WORKDIR", util.ReplaceNewlineWithSpaces(generator.yaml.Workdir))
		generator.writeEmptyLine()
	}
}

func (generator *dockerfileGenerator) writeCommands() error {
	for _, command := range generator.yaml.Commands {
		line, err := commands.ParseCommand(command, commands.MODE_RUN_INSTRUCTION)
		if err != nil {
			return err
		}
		generator.lines = append(generator.lines, line)
	}
	generator.writeEmptyLine()
	return nil
}

func (generator *dockerfileGenerator) writeEntrypoint() error {
	if generator.yaml.Entrypoint != nil {
		entrypoint, err := generator.yaml.ParseEntrypoint()

		if err != nil {
			return err
		}

		generator.write("ENTRYPOINT", entrypoint)
		generator.writeEmptyLine()
	}
	return nil
}

func (generator *dockerfileGenerator) writeLabels() {
	for key, value := range generator.yaml.Labels {
		line := fmt.Sprintf("\"%s\"=\"%s\"", util.EscapeQuotesAndNewline(key), util.EscapeQuotesAndNewline(value))
		generator.write("LABEL", line)
	}

	if len(generator.yaml.Labels) > 0 {
		generator.writeEmptyLine()
	}
}

func (generator *dockerfileGenerator) writeVariables() {
	for key, value := range generator.yaml.Variables {
		line := fmt.Sprintf("\"%s\"=\"%s\"", util.EscapeQuotesAndNewline(key), util.EscapeQuotesAndNewline(value))
		generator.write("ENV", line)
	}

	if len(generator.yaml.Variables) > 0 {
		generator.writeEmptyLine()
	}
}

func (generator *dockerfileGenerator) writeEmptyLine() {
	generator.lines = append(generator.lines, "")
}

func (generator *dockerfileGenerator) write(instruction string, value string) {
	line := fmt.Sprintf("%s %s", instruction, value)
	generator.lines = append(generator.lines, line)
}
