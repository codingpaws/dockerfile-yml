# dockerfile-yml

dockerfile-yml generates a `Dockerfile` from your `dockerfile.yml`, a YAML
version of the traditional [`Dockerfile`][dockerfile].

(You probably want to test this extensively before thinking about using it in a
productive environment. Please [report all bugs][new-issue] that you encounter,
so they can be fixed).

[new-issue]: https://gitlab.com/codingpaws/dockerfile-yml/-/issues/new

## Why?

Imagine this: you work in a `docker-compose.yml`, `.gitlab-ci.yml`, or any
other YAML file. And then you want to create a `Dockerfile`...

Now you have to adjust from the YAML-way of writing a file to the special
[`Dockerfile` way][dockerfile]. Isn’t that inconvenient?

More than that, it caps your productivity. One wrong colon here or wrong array
format there, and you got yourself [a failing CI pipeline][failing-ci]. But
don’t fret, dockerfile-yml is here to save your day and allows you to generate
a `Dockerfile` from your `dockerfile.yml`!

[dockerfile]: https://docs.docker.com/engine/reference/builder/
[failing-ci]: https://twitter.com/dnsmichi/status/1464274204359073800

## Installation

For linux, you can go to the [releases][releases] page and download the latest
binary. You can move it to `/usr/local/bin` to use it everywhere in your shell
with `dockerfile-yml`.

For other platforms, clone this repository and build the project with
`go build`.

[releases]: https://gitlab.com/codingpaws/dockerfile-yml/-/releases

## Usage

The `dockerfile-yml` binary takes a `dockerfile.yml` and converts it to a
`Dockerfile` in the same directory.

```bash
$ dockerfile-yml ./cool-project
✓ generated ./cool-project/Dockerfile (from ./cool-project/dockerfile.yml)
```

## Usage in CI

When building a docker image with CI, you can use the
[`codingpaws/dockerfile-yml`][docker-img] docker image. It includes the
`dockerfile-yml` binary. You can convert your `dockerfile.yml` on the fly like
you would on your local machine!

[docker-img]: https://hub.docker.com/r/codingpaws/dockerfile-yml

## Example

A full example is in the `example/` directory of this project. It is
conveniently used for integration testing this tool. The `dockerfile.yml` would
be converted to a `Dockerfile`. The expected `Dockerfile` contents are in the
`Dockerfile.expected` file in the same directory.

A simpler example to just print `Hello world` would be:

```yml
from: alpine:latest

commands:
  - cmd: echo "Hello world"
```
