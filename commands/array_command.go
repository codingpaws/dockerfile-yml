package commands

import (
	"encoding/json"
	"errors"
)

type ArrayCommand struct {
	args []string
}

func (command *ArrayCommand) ToString() (str string, err error) {
	if len(command.args) == 0 {
		err = errors.New("a command array must have at least the executable")
		return
	}

	bytes, err := json.Marshal(command.args)

	if err != nil {
		return
	}

	return string(bytes), nil
}
