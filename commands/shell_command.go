package commands

import "gitlab.com/codingpaws/dockerfile-yml/util"

type ShellCommand struct {
	Line string
}

func (command *ShellCommand) ToString() (string, error) {
	return util.ReplaceNewlineWithSpaces(command.Line), nil
}
