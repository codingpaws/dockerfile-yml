package commands_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/codingpaws/dockerfile-yml/commands"
)

func TestShellCommand(t *testing.T) {
	command := commands.ShellCommand{"apt \"install\"\n  php8.0"}
	str, err := command.ToString()
	assert.Nil(t, err)
	assert.Equal(t, "apt \"install\"   php8.0", str)
}
