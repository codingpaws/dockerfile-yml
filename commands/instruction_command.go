package commands

import (
	"fmt"
	"strings"
)

type InstructionCommand struct {
	line map[string]interface{}
}

func (command *InstructionCommand) ToString() (str string, err error) {
	err = command.validate()

	if err != nil {
		return
	}

	line := command.line[command.key()]
	str, err = ParseCommand(line, MODE_INLINE)

	str = strings.ToUpper(command.key()) + " " + str

	return
}

func (command *InstructionCommand) validate() error {
	count := 0
	for range command.line {
		count += 1
	}

	if count != 1 {
		return fmt.Errorf("instruction command can only have one key but got: %s", strings.Join(command.keys(), ", "))
	}

	return nil
}

func (command *InstructionCommand) keys() (keys []string) {
	for key := range command.line {
		keys = append(keys, key)
	}
	return
}

func (command *InstructionCommand) key() string {
	return command.keys()[0]
}

func getAllowedKeys() []string {
	return []string{
		"copy",
		"cmd",
	}
}
