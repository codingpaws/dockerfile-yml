package commands

import (
	"errors"
	"fmt"
	"reflect"

	"gitlab.com/codingpaws/dockerfile-yml/util"
)

const (
	MODE_INLINE CommandMode = iota
	MODE_RUN_INSTRUCTION
)

type CommandMode uint8

type Command interface {
	ToString() (string, error)
}

func ParseCommand(line interface{}, mode CommandMode) (str string, err error) {
	var command Command

	switch line.(type) {
	case string:
		command = &ShellCommand{line.(string)}
	case []interface{}:
		stringArray, err2 := util.InterfaceArrayToStringArray(line.([]interface{}))
		err = err2
		command = &ArrayCommand{stringArray}
	case map[string]interface{}:
		if mode == MODE_INLINE {
			err = errors.New("expected shell or array command but got instruction")
			break
		}
		command = &InstructionCommand{line.(map[string]interface{})}
	default:
		err = fmt.Errorf("command must be string or array but got %s", command)
	}

	if err != nil {
		return
	}

	str, err = command.ToString()

	if mode == MODE_RUN_INSTRUCTION {
		_type := reflect.TypeOf(command)
		if _type == reflect.TypeOf(&ShellCommand{}) || _type == reflect.TypeOf(&ArrayCommand{}) {
			str = "RUN " + str
		}
	}

	return
}
