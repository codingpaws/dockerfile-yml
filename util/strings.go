package util

import (
	"fmt"
	"reflect"
	"strings"
)

func EscapeQuotesAndNewline(str string) string {
	str = strings.ReplaceAll(str, "\"", "\\\"")
	str = strings.ReplaceAll(str, "\n", "\\n")
	return str
}

func ReplaceNewlineWithSpaces(str string) string {
	return strings.ReplaceAll(str, "\n", " ")
}

func InterfaceArrayToStringArray(input []interface{}) (output []string, err error) {
	for id, value := range input {
		if value == nil {
			err = fmt.Errorf("expected string but got nil in command #%d instead", id)
			return
		}
		valueType := reflect.TypeOf(value).Name()
		if valueType != "string" {
			err = fmt.Errorf("expected string but got %s in command #%d instead", valueType, id)
			return
		}
		output = append(output, value.(string))
	}
	return
}
