package util_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/codingpaws/dockerfile-yml/util"
)

func TestEscapeQuotesAndNewline(t *testing.T) {
	testCases := []struct {
		original string
		expected string
	}{
		{
			original: "\"Hello world!\"",
			expected: "\\\"Hello world!\\\"",
		},
		{
			original: "apt-get \"install\" \n  php8.0",
			expected: "apt-get \\\"install\\\" \\n  php8.0",
		},
	}
	for index, tC := range testCases {
		t.Run(fmt.Sprintf("%d", index), func(t *testing.T) {
			actual := util.EscapeQuotesAndNewline(tC.original)
			assert.Equal(t, tC.expected, actual)
		})
	}
}

func TestReplaceNewlineWithSpaces(t *testing.T) {
	testCases := []struct {
		original string
		expected string
	}{
		{
			original: "\"Hello world!\"",
			expected: "\"Hello world!\"",
		},
		{
			original: "apt-get \"install\" \n  php8.0",
			expected: "apt-get \"install\"    php8.0",
		},
	}
	for index, tC := range testCases {
		t.Run(fmt.Sprintf("%d", index), func(t *testing.T) {
			actual := util.ReplaceNewlineWithSpaces(tC.original)
			assert.Equal(t, tC.expected, actual)
		})
	}
}

func TestInterfaceArrayToStringArray(t *testing.T) {
	t.Run("with nil", func(t *testing.T) {
		x := []interface{}{nil}
		_, err := util.InterfaceArrayToStringArray(x)
		assert.Error(t, err)
		assert.Contains(t, err.Error(), "expected string but got nil in command #0 instead")
	})
	t.Run("with non-string", func(t *testing.T) {
		x := []interface{}{"hello", "apt-get update", 5}
		_, err := util.InterfaceArrayToStringArray(x)
		assert.Error(t, err)
		assert.Contains(t, err.Error(), "expected string but got int in command #2 instead")
	})
	t.Run("successful", func(t *testing.T) {
		x := []interface{}{"hello", "apt-get upgrade"}
		result, err := util.InterfaceArrayToStringArray(x)
		assert.Nil(t, err)
		assert.Equal(t, []string{"hello", "apt-get upgrade"}, result)
	})
}
