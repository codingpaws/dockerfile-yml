package util_test

import (
	"testing"

	"gitlab.com/codingpaws/dockerfile-yml/util"
)

func TestLogSuccessf(t *testing.T) {
	util.LogSuccessf("%s", "Test success")
}

func TestLogFailuref(t *testing.T) {
	util.LogFailuref("%s", "Test failure")
}
