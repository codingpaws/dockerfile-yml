package util

import (
	"fmt"

	"github.com/gookit/color"
)

func LogSuccessf(message string, options ...interface{}) {
	icon := color.FgGreen.Render("✓")
	logIconf(icon, message, options...)
}

func LogFailuref(message string, options ...interface{}) {
	icon := color.FgRed.Render("✗")
	logIconf(icon, message, options...)
}

func logIconf(icon string, message string, options ...interface{}) {
	fmt.Printf(fmt.Sprintf("%s %s\n", icon, message), options...)
}
