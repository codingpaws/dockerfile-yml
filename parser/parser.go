package parser

import (
	"fmt"
	"io/ioutil"

	"gitlab.com/codingpaws/dockerfile-yml/commands"
	"gopkg.in/yaml.v3"
)

func Parse(filename string) (dockerfile YamlDockerfile, err error) {
	bytes, err := ioutil.ReadFile(filename)

	if err != nil {
		return
	}

	err = yaml.Unmarshal(bytes, &dockerfile)

	if err != nil {
		return
	}

	err = dockerfile.validate()

	return
}

type YamlDockerfile struct {
	From       string
	Workdir    string
	Labels     map[string]string
	Variables  map[string]string
	Commands   []interface{}
	Entrypoint interface{}
}

func (file *YamlDockerfile) validate() error {
	if file.From == "" {
		return fmt.Errorf("expected 'from' tag in yaml")
	}

	return nil
}

func (file *YamlDockerfile) ParseEntrypoint() (string, error) {
	return commands.ParseCommand(file.Entrypoint, commands.MODE_INLINE)
}
